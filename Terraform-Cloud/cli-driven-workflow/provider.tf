terraform {
  cloud {
    organization = "prosperous-batch"
    workspaces {
      name = "prosperous-workspace1"
    }
  }
  ####
  required_version = ">= 1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }

  }
}



provider "aws" {
  region = var.region
  #profile = "default"
}



