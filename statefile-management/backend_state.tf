
terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "myprac2-tfstatefile"
    key    = "fder/terraform.tfstate"
    region = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "pros-prac-table"
  }
}